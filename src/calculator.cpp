#include <iostream>
#include <vector>
#include <string>

std::vector<std::string> ones {"","one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
std::vector<std::string> teens {"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen","sixteen", "seventeen", "eighteen", "nineteen"};
std::vector<std::string> tens {"", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
std::vector<std::string> input;
std::vector<std::string> first_number;
std::vector<std::string> last_number;
std::string command;
double first_int;
double last_int;
double convert(std::vector<std::string> numb);
void print_calculated(std::string command);

int main()
{
    std::string input_string;
    std::cout<<"Operators: minus, plus, times, over"<<std::endl;
    std::cout<<"Example input: three times twenty five"<<std::endl;
    std::cout<<"Input valid operations: ";

    while (true)
    {
        getline(std::cin,input_string);
        std::string word = "";

        //Putting all the separate words of the string into a vector
        for (size_t i = 0; i < input_string.length();i++) 
        {
            if (input_string[i] == ' ')
            {
                input.push_back(word);
                word = "";
            }
            else {
                word = word + input_string[i];
            }
        }
        input.push_back(word);//Adding the last word

        bool first_num = true;
        //Splitting the vector of input into three different parts
        for (int i = 0; i < input.size(); i++)
        {
            if (input[i] == "minus" || input[i] == "plus" || input[i] == "times" || input[i] == "over" || input[i] == "modulus")
            {
                command = input[i];
                first_num = false;
            }else if (first_num)
            {
                first_number.push_back(input[i]);
            }else{
                last_number.push_back(input[i]);
            }
        }
        //Converting the two worded numbers actual numbers 
        first_int = convert(first_number);
        last_int = convert(last_number);

        //Calculates and prints the result
        print_calculated(command);
        first_number.clear();
        last_number.clear();
        input.clear(); 
        std::cout<<"Enter a new calculation:  ";
    }
    return 0;
}

//Simply calculates the result based on the inputed operator
void print_calculated(std::string comm){
    double answer = 0;
    if (comm == "minus")
    {
        answer = first_int - last_int;
    }else if (comm == "plus")
    {
        answer = first_int + last_int;
    }else if (comm == "times")
    {
        answer = first_int * last_int;
    }else if (comm == "over")
    {
        answer = first_int / last_int;
    }
    
    std::cout<<"The answer is:  "<<answer<<std::endl; 
}

//Looks through the vectors of strings until it matches and performs simple calulations to get correct value based on index
double convert(std::vector<std::string> number){
    double ten = 0;
    double one = 0;
    double teen = 0;
    for(auto i: number){
        for(int j = 0; j < ones.size(); j++){
            if(i == ones[j]){
                one = j;
            }else if (i == tens[j])
            {
                ten = j * 10;
            }else if (i == teens[j])
            {
                teen = j + 10;
            }
        }
    }
    ten += (one + teen);
    return ten;
}