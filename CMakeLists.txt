# set cmake minimum version
cmake_minimum_required(VERSION 3.10)

# set the project name and version
project(Alternate_Caps_Project VERSION 1.0)

# include and link source folder
include_directories(src)
link_directories(src)

# add the executable
add_executable(calc src/calculator.cpp)

# add add library
add_library(Alternate_Caps_Lib src/calculator.cpp)

# link the library to the executable
target_link_libraries(calc Alternate_Caps_Lib)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# make release the default build
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
  message(STATUS "Build type not specified: Use Release by default")
endif()

set(CMAKE_CXX_FLAGS "-Wall -Wextra")
set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_CXX_FLAGS_RELEASE "-O")