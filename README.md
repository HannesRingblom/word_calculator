# Word Calculator

Takes worded calculation as input and returns the answer in numbers

## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Maintainers](#maintainers)

## Requirements

Requires `cmake` and `gcc`.

## Usage

Uses Cmake
```sh
# Create build directory and move into it
$ mkdir -p build && cd build
# Generate makefile using Cmake - default is release
$ cmake ../     
# Compile the project
$ make
#Run the compiled binary
$ ./calc
```
Example input for each command:
```sh
¤ Valid input: 1-99
# Minus(-)
¤ thirty five minus seventy eight, returns: -43
¤ eighty six minus fifty nine, returns: 27
# Plus(+)
¤ sixteen plus nine, returns: 25
¤ twenty four plus sixty seven, returns: 91    
# Over(/)
¤ eighty four over twenty one, returns: 4
¤ seventeen over ninety two, returns: 0.184783
# Times(x)
¤ ninety nine times ninety nine, returns: 9801
¤ four times six, returns: 24
```

## Maintainers

[Hannes Ringblom (@HannesRingblom)](https://gitlab.com/HannesRingblom)